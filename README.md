# yurico

Unofficial set of experimental libraries and CLI tools to do Yubico OTP operations in Python.

## Installation

- Have a copy of python3.6+.
- Install dependencies: `pip3 install -Ur requirements.txt`.
- That's all, you're good to go.

## Components

- [fren](https://youtu.be/iePpY04E-gM?t=4376): OTP Key Generator
- [snusk](https://youtu.be/dvln4gDcUnk?t=176): OTP Generator
- [agga](https://youtu.be/-G5FdQrfwLw): OTP Decoder and Parser
- [kupteraz](https://youtu.be/W9DST-6jIBU?t=424): YubiCloud verifier

All of these components can be used as both CLI tools and as libraries. Currently, no documentation is provided for using them as libraries. CLI tools also have various environment variables that can be used in place of the arguments and options. Documentation is also currently not provided for these.

## Example CLI usage

One can generate keys with `fren`:

```bash
$ python3 fren.py
Public Identity: vvjekdldvknt
Private Identity: ec54bfcb4843
Secret Key: 19d72b7f5355f9ec9cd7eacff983c618
```

Then generate an OTP code using this information with `snusk`:

```bash
$ python3 snusk.py vvjekdldvknt ec54bfcb4843 19d72b7f5355f9ec9cd7eacff983c618 1 0
vvrjvrkhgbrniutlglvclbtvikkggdjnlbjbtugifj
```

And parse this using `agga`:

```bash
$ python3 agga.py vvjekdldvkntcrkftbdkdjdeddnntvubknvbkhtujkin 19d72b7f5355f9ec9cd7eacff983c618
Public Identity: vvjekdldvknt
Private Identity: ec54bfcb4843
Session Counter: 1
Timestamp: 12888
Use Counter: 0
Random Number: 18642
Checksum: 7e77
Checksum passes: True
```

Note: Timestamp is randomly generated and as such does not properly reflect a real Yubikey's actions.

This key can then be submitted to YubiCloud at https://upload.yubico.com/ (any serial works, example: 7800014), and another OTP code with a higher session/use count can then be verified with `kupteraz`:

```bash
$ python3 snusk.py vvjekdldvknt ec54bfcb4843 19d72b7f5355f9ec9cd7eacff983c618 7 0
vvjekdldvkntfulthgdgggvbigfnfcrbbhdttdjuturn
$ python3 kupteraz.py vvjekdldvkntfulthgdgggvbigfnfcrbbhdttdjuturn
Success: True
```

**Note:** By default, `kupteraz` uses an unidentified API client with Client ID 1. You can get a proper Client ID and Client Secret at https://upgrade.yubico.com/getapikey/ for YubiCloud. This can then be supplied to kupteraz with `--apiclientid` and `--apiclientsecret` respectively.

## Disclaimer

Uploading your key to Yubicloud or getting a Yubicloud API key may be a violation Yubico's ToS. ("may", as I am too lazy to read it.)