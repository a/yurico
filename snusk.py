from Cryptodome.Cipher import AES
import struct
import random
from yurico_helpers import (
    hex_to_modhex,
    calc_yubico_otp_checksum,
    re_public_id,
    re_priv_id,
    re_secret_key,
    print_bold,
)


def generate_yubico_otp(
    pub_identity: str,
    priv_identity: str,
    secret_key: str,
    session_counter: int,
    use_counter: int,
    timestamp=None,
    random_number=None,
):
    out = pub_identity

    if not timestamp:
        timestamp = random.randint(0, 0xFFFFFF)

    if not random_number:
        random_number = random.randint(0, 0xFFFF)

    # Example: ca5f3a413ec601001e90a10b16173d53
    # ca5f3a413ec6: Private Identity
    # 0100: LE session counter
    # 1e90a1: LE "timestamp"
    # 0b: LE use counter
    # 1617: random number
    # 3d53: checksum for past 14 bytes
    priv_struct = struct.pack(
        "<6sH3sB2s",
        bytearray.fromhex(priv_identity),
        session_counter,
        struct.pack("<I", timestamp)[1:],  # hack
        use_counter,
        struct.pack("<H", random_number),
    )
    calculated_checksum = calc_yubico_otp_checksum(priv_struct)
    priv_data = priv_struct + calculated_checksum

    # Encrypt data
    cipher = AES.new(bytearray.fromhex(secret_key), AES.MODE_ECB)
    data = cipher.encrypt(priv_data)

    # modhex data, append to OTP output
    out += hex_to_modhex(data.hex())
    return {"otp": out}


if __name__ == "__main__":
    import re
    import click
    import json

    @click.command()
    @click.argument("public_id", envvar="YURICO_PUBID")
    @click.argument("private_id", envvar="YURICO_PRIVID")
    @click.argument("secret_key", envvar="YURICO_SECRET")
    @click.argument("session_counter", type=int, envvar="YURICO_SESSION_COUNT")
    @click.argument("use_counter", type=int, envvar="YURICO_USE_COUNT")
    @click.option(
        "--timestamp",
        help="Timestamp (number up to 16777215)",
        type=int,
        envvar="YURICO_TIMESTAMP",
    )
    @click.option(
        "--random_number",
        help="Random number (number up to 65535)",
        type=int,
        envvar="YURICO_RANDOM",
    )
    @click.option(
        "--jsonoutput", help="JSON output", is_flag=True, envvar="YURICO_JSON"
    )
    def cli_generate_otp(
        public_id,
        private_id,
        secret_key,
        session_counter,
        use_counter,
        timestamp=None,
        random_number=None,
        jsonoutput=False,
    ):
        if not re.fullmatch(re_public_id, public_id):
            print_bold(
                "Error: Public identity does not match the required format.",
                stderr=True,
            )
            return
        if not re.fullmatch(re_priv_id, private_id):
            print_bold(
                "Error: Private identity does not match the required format.",
                stderr=True,
            )
            return
        if not re.fullmatch(re_secret_key, secret_key):
            print_bold(
                "Error: Secret key does not match the required format.", stderr=True
            )
            return

        out = generate_yubico_otp(
            public_id, private_id, secret_key, session_counter, use_counter
        )
        if jsonoutput:
            print(json.dumps(out))
        else:
            print(out["otp"])

    cli_generate_otp()
