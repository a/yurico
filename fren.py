import secrets
from yurico_helpers import (
    hex_to_modhex,
    fancy_dict_print,
    re_public_id,
    re_priv_id,
    print_bold,
)


def generate_identity_and_secret(public_identity=None, priv_identity=None) -> dict:
    if not public_identity:
        public_identity = "vv" + hex_to_modhex(secrets.token_hex(5))

    if not priv_identity:
        priv_identity = secrets.token_hex(6)

    secret_key = secrets.token_hex(16)

    result = {
        "public_identity": public_identity,
        "private_identity": priv_identity,
        "secret_key": secret_key,
    }

    return result


if __name__ == "__main__":
    import re
    import click
    import json

    @click.command()
    @click.option(
        "--publicid",
        help="Public identity (0-16 chars, characters: cbdefghijklnrtuv)",
        envvar="YURICO_PUBID",
    )
    @click.option(
        "--privid", help="Private identity (16 character hex)", envvar="YURICO_PRIVID"
    )
    @click.option(
        "--jsonoutput", help="JSON output", is_flag=True, envvar="YURICO_JSON"
    )
    def cli_generate_key(publicid=None, privid=None, jsonoutput=False):
        if publicid:
            if not re.fullmatch(re_public_id, publicid):
                print_bold(
                    "Error: Public identity does not match the required format.",
                    stderr=True,
                )
                return
            if len(publicid) != 12:
                print_bold(
                    "Warning: Public ID length is not 12 characters.", stderr=True
                )
                print_bold("It will not be usable with YubiCloud.", stderr=True)

        if privid and not re.fullmatch(re_priv_id, privid):
            print_bold(
                "Error: Private identity does not match the required format.",
                stderr=True,
            )
            return

        out = generate_identity_and_secret(publicid, privid)
        if jsonoutput:
            print(json.dumps(out))
        else:
            fancy_dict_print(out)

    cli_generate_key()
