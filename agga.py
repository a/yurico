from Cryptodome.Cipher import AES
import struct
from yurico_helpers import (
    modhex_to_hex,
    calc_yubico_otp_checksum,
    le_bytes_to_int,
    fancy_dict_print,
    re_secret_key,
    re_otp,
    print_bold,
)


def parse_yubico_otp(otp: str, secret_key: str, debug: bool = False) -> dict:
    public_identity = otp[0:-32]
    priv_str_enc = modhex_to_hex(otp)[-32:]
    priv_hex_enc = bytearray.fromhex(priv_str_enc)
    cipher = AES.new(bytearray.fromhex(secret_key), AES.MODE_ECB)
    priv_hex = cipher.decrypt(priv_hex_enc)

    yk_record = struct.unpack("<6sH3sB2s2s", priv_hex)

    calculated_checksum = calc_yubico_otp_checksum(priv_hex)
    checksum_passes = yk_record[5] == calculated_checksum

    result = {
        "public_identity": public_identity,
        "private_identity": yk_record[0].hex(),
        "session_counter": yk_record[1],
        "use_counter": yk_record[3],
        "timestamp": le_bytes_to_int(yk_record[2]),
        "random_number": le_bytes_to_int(yk_record[4]),
        "checksum": yk_record[5].hex(),
        "checksum_calculated": calculated_checksum.hex(),
        "checksum_valid": checksum_passes,
    }

    if debug:
        result["unmodhexed_private_section"] = priv_str_enc
        result["decrypted_private_section"] = priv_hex.hex()

    return result


if __name__ == "__main__":
    import re
    import click
    import json

    @click.command()
    @click.argument("otp", envvar="YURICO_OTP")
    @click.argument("secret_key", envvar="YURICO_SECRET")
    @click.option(
        "--debug", help="Include additional data", is_flag=True, envvar="YURICO_DEBUG"
    )
    @click.option(
        "--jsonoutput", help="JSON output", is_flag=True, envvar="YURICO_JSON"
    )
    def cli_parse_otp(otp, secret_key, debug=False, jsonoutput=False):
        if not re.fullmatch(re_otp, otp):
            print_bold("Error: OTP does not match the required format.", stderr=True)
            return
        if not re.fullmatch(re_secret_key, secret_key):
            print_bold(
                "Error: Secret key does not match the required format.", stderr=True
            )
            return

        parse_result = parse_yubico_otp(otp, secret_key, debug)
        if jsonoutput:
            print(json.dumps(parse_result))
        else:
            fancy_dict_print(parse_result)

    cli_parse_otp()
